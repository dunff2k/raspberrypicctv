# README #

This repository is a project to make a Raspberry Pi focused CCTV platform.

### How do I get set up? ###

This is early days but the platform is currently based upon 2 scripts for installation on the Raspberry Pi.

* surv- Performs the data capture from the Raspberry Pi camera
* upload- Performs the uploads to the web server of your choice

### Who do I talk to? ###

* This project is owned by Paul Dunphy. Get in touch if you wish to contribute.